<?php
// Fonction ajout d'une question et d'une réponse
$ajout = '';

if($_POST && !empty($_POST['ajquestion']) && !empty($_POST['ajreponse'])) {
  $ajquestion = $_POST['ajquestion'];
  $ajreponse = $_POST['ajreponse'];
  $ajreponse =  mb_strtoupper($ajreponse);

  global $wpdb;
  $nom_table = $wpdb->prefix . "capcodac_plugin";
  $insert = "INSERT INTO  $nom_table (question, reponse) VALUES ('$ajquestion','$ajreponse')";
  $result = $wpdb->query($insert);
    //Fonction vérification
    if (!empty($ajquestion. $ajreponse)) {
        $ajout = '<div class="msg-ok">Votre ajout a été effectué</div>';
      }
      else {
        $ajout = '<div class="msg-error">Veuillez recommencer !</div>';
      }
}
  //Fonction modification d'une question
if (!empty($_POST['newquestion'])) {
  $newquestion = $_POST['newquestion'];
  $newquestionid = $_POST['oldquestion'];

  global $wpdb;
  $nom_table = $wpdb->prefix . "capcodac_plugin";

  $wpdb->query('UPDATE '.$nom_table.' SET question = "'.$newquestion.'" WHERE id = '.$newquestionid);
    //Fonction vérification
    if (!empty($newquestion) && $newquestion != $questionlist) {
        $remplace = '<div class="msg-ok">La question a été modifiée</div>';
      }
}

  //Fonction Suppression d'une question
if (isset($_POST['supprimer'])) {
  $suppressionid = $_POST['supprimer'];

  global $wpdb;
  $nom_table = $wpdb->prefix . "capcodac_plugin";
  $wpdb->query('DELETE FROM '.$nom_table.' WHERE id = '.$suppressionid);
    //Fonction vérification
    if(!empty($suppressionid)) {
      $suppr= '<div class="msg-ok">Votre question a été supprimé</div>';
    }
}

global $wpdb;
$nom_table = $wpdb->prefix . "capcodac_plugin";
$question = "SELECT id, question, reponse  FROM wp_capcodac_plugin";
$result = $wpdb->get_results($question);
?>
<section class="capcodac_plugin">
  <h2> Ajout d'une question/réponse pour le captcha </h2>

    <div class="ajout">
      <form action="#" method="post">
          <div class="presentation-row row">
            <div class="presentation-text col-xs-12 col-md-4">
              <label for="ajquestion">
                <input type="text" id="ajquestion" name="ajquestion" placeholder="Votre question" maxlength="50" required>
            </div>
            <div class="presentation-text col-xs-12 col-md-4">
              <label for="ajreponse">
                <input type="text" id="ajreponse" name="ajreponse" placeholder="Votre réponse" maxlength="15" required>
            </div>
            <div class="presentation-text col-xs-12 col-md-2">
              <button type="submit">Valider</button>
          </div>
        </div>
      </form>
        <div class="verif"><?= $ajout ?></div>
    </div>
    <!-- Afficher les questions et réponses existantes -->

  <h3> Liste des questions/réponses existantes :</h3>
    <div class="affquestion">
  	   <div class="list presentation-row row">
  				<div class="listquest presentation-text col-12 col-md-4">
            <?php for($i=0; $i<count($result); $i++){
            echo '- '.$result[$i]->question.'<br>';
      }?>
          </div>
         <div class="listrep presentation-text col-12 col-md-2">
           <?php for($i=0; $i<count($result); $i++){
                 echo $result[$i]->reponse.'<br>';
           }?>
        </div>
      </div>
    </div>
    <hr class="ligne">
</section>
<section class="capcodac_plugin">
  <h2>modification d'une question </h2>
      <div class="modification">
      <?php

        $affiche = '';

        for ($i= 0; $i < count($result); $i++) {
         $affiche .= '<option value="'.$result[$i]->id.'">'.$result[$i]->question.'<br> </option>';
         $questionlist = $result[$i]->question;
        }  ?>

      <form action="#" method="post">
    	   <div class="old presentation-row row">
           <div class="presentation-text col-xs-12 col-md-4">
              <select name="oldquestion">
                <?= $affiche ?>
              </select>
          </div>
          <div class="presentation-text col-xs-12 col-md-4">
            <label for="newquestion">
              <input type="text" id="newquestion" name="newquestion" placeholder="Nouvelle question" maxlength="100" required>
          </div>
          <div class="presentation-text col-xs-12 col-md-2">
             <button type="submit">Modifier</button>
         </div>
     </form>
          <div class="verif">  <?= $remplace ?></div>
        </div>
</section>
        <hr class="ligne">

    <h2>Suppression d'une question </h2>

       <section class="capcodac_plugin">
           <form action='#' method="post">
             <div class="suppr presentation-row row">
              	<div class="presentation-text col-xs-12 col-md-8">
                    <select name="supprimer">
                      <?= $affiche ?>
                    </select>
                      <div class="verif"> <?= $suppr ?></div>
                </div>
                <div class="presentation-text col-xs-12 col-md-2">
                  <button type="submit">Supprimer</button>
                </div>
            </div>
          </form>
      </section>
