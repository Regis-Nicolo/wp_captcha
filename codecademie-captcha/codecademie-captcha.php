<?php
/*
Plugin Name: Captcha Code-academie
Plugin URI: http://code-academie.fr
Description: Un captcha question-reponse
Version: 0.1
Author: Régis Nicolo - Erwann Duclos @Docusland
Author URI: http://gitlab.com/Regis-Nicolo
*/

//Fonction insertion
function database_prefill() {

  global $wpdb;
  $nom_table = $wpdb->prefix . "capcodac_plugin";

  $wpdb->insert(
  		$nom_table,
  		array(
  			'question' => 'Quelle est la plus grande ville française ?',
  			'reponse' => 'PARIS'
  		),
      array(
        '%s',
        '%s'
      )
  	);

    $wpdb->insert(
  		$nom_table,
  		array(
  			'question' => 'Quelle est la couleur du ciel ?',
  			'reponse' => 'BLEU'
  		),
      array(
        '%s',
        '%s'
      )
  	);

    $wpdb->insert(
  		$nom_table,
  		array(
  			'question' => 'Retranchez deux à sept',
  			'reponse' => 'CINQ'
  		),
      array(
        '%s',
        '%s'
      )
    );
}

//dbDelta($sql);

function database_install(){
global $wpdb;
$nom_table = $wpdb->prefix . "capcodac_plugin";
$sql = "CREATE TABLE $nom_table(
  id INTEGER(2) UNSIGNED AUTO_INCREMENT,
  question varchar(100) NOT NULL,
  reponse varchar(20) NOT NULL,
  PRIMARY KEY  (id)
)";
dbDelta($sql);
}

require_once(ABSPATH . 'wp-admin/includes/upgrade.php' );

register_activation_hook( __FILE__, 'database_install' );
register_activation_hook( __FILE__, 'database_prefill' );

/***************************SHORTCODE***************/

add_shortcode('quizz_captcha', 'capcodac_shortcode');
function capcodac_shortcode(){
  ob_start();


  global $wpdb;
  $nom_table = $wpdb->prefix . "capcodac_plugin";
  $question = "SELECT * FROM wp_capcodac_plugin ORDER BY rand() LIMIT 1";
  $dbresult = $wpdb->get_results($question);
  $captcha_question = $dbresult[0]->question;
  $captcha_reponse = $dbresult[0]->reponse;



//$captcha_reponse = $_POST["$captcha_reponse"];
  include('shortcode/captcha.php');
  return ob_get_clean();
}

session_start();

add_action('admin_menu', 'add_menu_plugin');

function add_menu_plugin() {
     add_menu_page(
        'ajout question',
        'Configuration captcha',
        'manage_options',
        'code_ac_ajout_question',
        'displayFormQuestion'
    );
}
 function displayFormQuestion () {
      include('ajout_question.php');
 }


/********************STYLES*******************************/
wp_enqueue_style('plugin-stylesheet', plugins_url('styles.css', __FILE__));

/*******************BOOTSTRAP****************************/
wp_enqueue_style('plugin-bootstrap', plugins_url('bootstrap.min.css', __FILE__));

?>
